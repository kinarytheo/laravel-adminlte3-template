# laravel AdminLTE 3 Template - Starter Page

## Installation

1.  Clone repository

    `git clone https://gitlab.com/kinarytheo/laravel-adminlte3-template.git`

2.  Copy **.env.example** to **.env**
3.  run `composer install`
4.  run `php artisan key:generate`
5.  run `php artisan migrate`
6.  run `npm i`
7.  run `npm run dev`
8.  run `php artisan serve`