<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>

        <!-- Left navbar menu opional -->
        {{-- <li class="nav-item d-none d-sm-inline-block">
            <a href="index3.html" class="nav-link">Home</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="#" class="nav-link">Contact</a>
        </li> --}}
    </ul>

    <!-- SEARCH FORM -->
    <form action="" class="w-100">
    <input class="form-control form-control-sm form-control-dark w-100" type="search" placeholder="Search" aria-label="Search">
    </form>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-bell"></i>
            <span class="badge badge-warning navbar-badge">15</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-header">15 Notifications</span>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> 4 new messages
            <span class="float-right text-muted text-sm">3 mins</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
            <i class="fas fa-users mr-2"></i> 8 friend requests
            <span class="float-right text-muted text-sm">12 hours</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
            <i class="fas fa-file mr-2"></i> 3 new reports
            <span class="float-right text-muted text-sm">2 days</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div>
        </li>

        <!-- User Dropdown Menu -->
        <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-user"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <div class="dropdown-header">
                <strong>{{ auth()->user()->name }}</strong>
                <div class="text-gray-500">{{ auth()->user()->email }}</div>
            </div>
            
            <div class="dropdown-divider"></div>
            
            <a href="#" class="dropdown-item">
                <i class="fas fa-user mr-2"></i> Profile
            </a>
            <a href="#" class="dropdown-item">
                <i class="fas fa-cog mr-2"></i> Setting
            </a>
            
            <div class="dropdown-divider"></div>
            
            <a href="#" class="dropdown-item">
                <i class="fas fa-file mr-2"></i> data
                <span class="float-right text-muted text-sm">10 data</span>
            </a>

            <div class="dropdown-divider"></div>
            
            <a href="#" class="dropdown-item">
                <i class="fas fa-users mr-2"></i> User Management
            </a>
            
            <div class="dropdown-divider"></div>
            
            <a class="dropdown-item bg-danger" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                <i class="fas fa-sign-out-alt fa-fw"></i> {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>
        </li>
        <li class="nav-item">
        {{-- <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#">
            <i class="fas fa-th-large"></i></a>
        </li> --}}
    </ul>
</nav>
<!-- /.navbar -->